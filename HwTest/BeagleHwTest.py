#!/bin/evn python

import time
import serial
import os

#import Adafruit_BBIO.UART as UART

import LCD

def encoderBCallback(msg):
    print "Encoder B toggled:", msg

def encoderACallback(msg):
    print "Encoder A toggled:", msg


class PushButton(object):
    def __init__(self, iPathEnvVar, iName, iIsInput, iCallback=None):
        self.valuePath = os.getenv(iPathEnvVar, "/tmp/foobar")
        print "Setting up GPIO with path of", self.valuePath
        self.name = iName
        self.isInput = iIsInput
        self.prevState = False
        self.callback = iCallback

    def setState(self, iState):
        if self.isInput:
            raise Exception("Invalid Command")
        else:
            with open(self.valuePath, 'w') as f:
                f.write("1" if iState else "0")

    def registerCallback(self, iCallback):
        self.callback = iCallback

    def getChanged(self):
        with open(self.valuePath, 'r') as f:
            state = f.read(1)

            if state == '':
                return (False, self.prevState)

            state = int(state)

        if state != self.prevState:
            self.prevState = state
            return (True, state)
        else:
            return (False, state)
        
    def getState(self):
        with open(self.valuePath, 'r') as f:
            state = int(f.read(1))
            
        if state != self.prevState:
            self.prevState = state
            print self.name, "state changed to", "True" if state else "False"
            if self.callback:
                self.callback(state)

        return state

buttons = { "U":PushButton("UP_BUTTON_PATH", "UP", True), 
            "D":PushButton("DOWN_BUTTON_PATH", "DOWN", True), 
            "L":PushButton("LEFT_BUTTON_PATH", "LEFT", True), 
            "R":PushButton("RIGHT_BUTTON_PATH", "RIGHT", True), 
            "S":PushButton("SELECT_BUTTON_PATH", "SELECT", True),
            "P":PushButton("ENCODER_PUSH_PATH", "Encode_SW", True) }
#            "B":PushButton("P9_21", "Encode_B", True),
#            "A":PushButton("P9_41", "Encode_A", True) }

outputs = { "HR":PushButton("HEAT_OFF_RELAY_PATH", "Heat_R+", False),
            "CR":PushButton("COOL_OFF_RELAY_PATH", "Cool_R+", False),
            "FR":PushButton("FAN_OFF_RELAY_PATH", "Fan_R+", False),
            "HS":PushButton("HEAT_RELAY_PATH", "Heat_S+", False),
            "CS":PushButton("COOL_RELAY_PATH", "Cool_S+", False),
            "FS":PushButton("FAN_RELAY_PATH", "Fan_S+", False),
            "Red":PushButton("LED_RED_PATH", "LED_Red", False),
            "Green":PushButton("LED_GREEN_PATH", "LED_Green", False),
            "Blue":PushButton("LED_BLUE_PATH", "LED_Blue", False) }

try:
    #UART.setup("UART1")
    #ser = serial.Serial(port="/dev/ttyO1", baudrate=9600)
    #time.sleep(5)
    #display = LCD.LCD(ser)
    #display.clear()

    

    buttonKeys = buttons.keys()
    while True:
        count = 0
        #display.clear()
        for letter in buttonKeys:
            #state = buttons[letter].getState()
            changed, state = buttons[letter].getChanged()
            #print "State is:", state
            if changed:
                if state == 1:
                    print "open %s to %d"%(letter, count)
                    #display.writeText(letter, count, 0)
                else:
                    print "ground %s to %d"%(letter, count)
                    #display.writeText(" ", count, 0)

                if letter == "U":
                    outputs["Red"].setState(state)
                    outputs["HS"].setState(state)
                    outputs["HR"].setState(not state)
                elif letter == "D":
                    outputs["Green"].setState(state)
                    outputs["FS"].setState(state)
                    outputs["FR"].setState(not state)
                elif letter == "S":
                    outputs["Blue"].setState(state)
                    outputs["CS"].setState(state)
                    outputs["CR"].setState(not state)

            count += 1
        time.sleep(0.001)

except Exception, e:
    print e

#ser.close()

