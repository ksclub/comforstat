import pyinotify
import time

import Environment as env

class EventHandler(pyinotify.ProcessEvent):
    def process_IN_MODIFY(self, event):
        print "Modifying:", event.pathname

    def process_IN_CREATE(self, event):
        print "Creating:", event.pathname

    def process_IN_DELETE(self, event):
        print "Removing:", event.pathname

def quick_check(notifier):
    assert notifier._timeout is not None, 'Notifier must be constructed with a short timeout'
    notifier.process_events()
    while notifier.check_events():  #loop in case more events appear while we are processing
        notifier.read_events()
        notifier.process_events()


# The watch manager stores the watches and provides operations on watches
wm = pyinotify.WatchManager()
mask = pyinotify.IN_MODIFY | pyinotify.IN_DELETE | pyinotify.IN_CREATE  # watched events
wm.add_watch(env.HeatRelayPath, mask, rec=True)
wm.add_watch(env.CoolRelayPath, mask, rec=True)
wm.add_watch(env.FanRelayPath, mask, rec=True)

handler = EventHandler()
the_notifier = pyinotify.Notifier(wm, handler, timeout=10)

while True:
    time.sleep(0.1)
    quick_check(the_notifier)
