import os
import wx
import thread
import time
import datetime
import socket

import pyinotify

import Environment as env
import LedIndicator

gEncoderPos=0

class EventHandler(pyinotify.ProcessEvent):
    def __init__(self, callback):
        self.callback = callback
        
    def process_IN_MODIFY(self, event):
        self.callback(event.pathname)

    def process_IN_CREATE(self, event):
        print "Creating:", event.pathname

    def process_IN_DELETE(self, event):
        print "Removing:", event.pathname

class TestFrame(wx.Frame):
    def __init__(self):
        self.setupWxGui()
        self.OnCurrentTempChange(None) # trigger file to be created if it doesn't exist

        # setup file listeners
        wm = pyinotify.WatchManager()
        mask = pyinotify.IN_MODIFY | pyinotify.IN_DELETE | pyinotify.IN_CREATE  # watched events
        wm.add_watch(env.HeatRelayPath, mask)
        wm.add_watch(env.CoolRelayPath, mask)
        wm.add_watch(env.FanRelayPath, mask)
        wm.add_watch(env.LedRedPath, mask)
        wm.add_watch(env.LedGreenPath, mask)
        wm.add_watch(env.LedBluePath, mask)

        self.handler = EventHandler(self.onFileUpdate)
        self.notifier = pyinotify.Notifier(wm, self.handler, timeout=10)

        # clear display
        self.Display.WriteText("                \n                ")
        self.setPos(0, 0)
        
        # set up timer to update time
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.onTimer, self.timer)        
        self.timer.Start(500)

        # set up pipe between LCD and application
        if not os.path.exists(env.UartPath):
            os.mkfifo(env.UartPath)

        self.pipe = os.open(env.UartPath, os.O_RDONLY | os.O_NONBLOCK)

    def setPos(self, iX, iY):
        self.Display.SetInsertionPoint(self.Display.XYToPosition(iX, iY))

    def onTimer(self, event):
        self.checkFileUpdates()
        self.checkUartData()

    def checkFileUpdates(self):
        assert self.notifier._timeout is not None, 'Notifier must be constructed with a short timeout'
        self.notifier.process_events()
        while self.notifier.check_events():  #loop in case more events appear while we are processing
            self.notifier.read_events()
            self.notifier.process_events()

    def checkUartData(self):
        while True:
            byte = None

            # see if there's data
            try:
                byte = os.read(self.pipe, 1)
            except:
                # nothing to read so exit
                return

            if not byte:
                # must have been no data
                break

            if byte != '\xFE' and byte != '\x7c':
                try:
                    byte.decode('ascii')
                except UnicodeDecodeError:
                    print "Got a bad byte (0x%02x)"%ord(byte)
                    continue
                pos = self.Display.GetInsertionPoint()
                x, y = self.Display.PositionToXY(pos)
                move = False

                if x == 16:
                    move = True
                    x = 0
                    y += 1

                if y == 2:
                    move = True
                    y = 0

                if move:
                    pos = self.Display.XYToPosition(x, y)

                self.Display.Replace(pos, pos + 1, byte)
            else:
                # it's not ASCII, lets see if it's a special character
                if byte == '\xFE':
                    byte = None
                    while not byte:
                        try:
                            byte = os.read(self.pipe, 1)
                        except:
                            pass

    
                    if byte == '\x01': # clear
                        self.Display.Clear()
                        self.Display.WriteText("                \n                ")
                    elif byte == '\x14': # right one
                        pass
                    elif byte == '\x10': # left one
                        pass
                    elif byte >= '\x80': # set position
                        pos = ord(byte) - 0x80
                        if pos >= 0 and pos <= 15:
                            self.setPos(pos, 0)
                        elif pos >= 64 and pos <= 79:
                            self.setPos(pos - 64, 1)
                elif byte == '\x7C':
                    pass

    def readByte(self):
        byte = None

        while not byte:
            byte = self.pipe.read(1)

        return byte

    def resetSelect(self):
        with open(env.EncoderPushPath, 'w') as f:
            f.write("0")

    def onSelect(self, event):
        with open(env.EncoderPushPath, 'w') as f:
            f.write("1")
        wx.FutureCall(500, self.resetSelect)

    def onLeft(self, event):
        global gEncoderPos

        gEncoderPos -= 4
        with open(env.EncoderKnobPath, 'w') as f:
            f.write(str(gEncoderPos))

    def onRight(self, event):
        global gEncoderPos

        gEncoderPos += 4
        with open(env.EncoderKnobPath, 'w') as f:
            f.write(str(gEncoderPos))

    def resetHome(self):
        with open(env.HomeButtonPath, 'w') as f:
            f.write("0")

    def onHome(self, event):
        with open(env.HomeButtonPath, 'w') as f:
            f.write("1")
        wx.FutureCall(500, self.resetHome)

    def resetBack(self):
        with open(env.BackButtonPath, 'w') as f:
            f.write("0")

    def onBack(self, event):
        with open(env.BackButtonPath, 'w') as f:
            f.write("1")
        wx.FutureCall(500, self.resetBack)

    def OnCurrentTempChange(self, event):
        with open(env.CurrentTempPath, 'w') as f:
            f.write(str(self.CurrentTemp.GetValue()))

    def onFileUpdate(self, filePath):
        if filePath in env.CoolRelayPath:
            with open(env.CoolRelayPath, 'r') as f:
                value = f.read(100)
            self.CoolIndicator.SetState(int(value))
        elif filePath in env.FanRelayPath:
            with open(env.FanRelayPath, 'r') as f:
                value = f.read(100)
            self.FanIndicator.SetState(int(value))
        elif filePath in env.HeatRelayPath:
            with open(env.HeatRelayPath, 'r') as f:
                value = f.read(100)
            self.HeatIndicator.SetState(int(value))
        elif filePath in env.LedRedPath or filePath in env.LedGreenPath or filePath in env.LedBluePath:
            with open(env.LedRedPath, 'r') as f:
                redValue = f.read(100)
            with open(env.LedGreenPath, 'r') as f:
                greenValue = f.read(100)
            with open(env.LedBluePath, 'r') as f:
                blueValue = f.read(100)

            self.ThermostatLed.SetState(int(redValue) * 4 + int(greenValue) * 2 + int(blueValue))
        else:
            print "Unknown file modified:", filePath

    def setupWxGui(self):
        wx.Frame.__init__(self, parent=None, id=-1, title="Testing", pos=(350, 110), size=(490,200), style=wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX | wx.MINIMIZE_BOX)
        panel = wx.Panel(self)

        # encoder buttons
        self.LeftButton = wx.Button(parent=panel, id=-1, label="Left", pos=(60, 17), size=(50, 20))
        self.SelectButton = wx.Button(parent=panel, id=-1, label="Select", pos=(110, 17), size=(50, 20))
        self.RightButton = wx.Button(parent=panel, id=-1, label="Right", pos=(160, 17), size=(50, 20))

        # home/back button
        self.HomeButton = wx.Button(parent=panel, id=-1, label="Home", pos=(60, 37), size=(50, 20))
        self.BackButton = wx.Button(parent=panel, id=-1, label="Back", pos=(160, 37), size=(50, 20))

        # temp control
        self.CurrentTemp = wx.SpinCtrl(parent=panel, value='68', pos=(240, 17), size=(60, -1), min=0, max=100)
    
        # relay indicators
        wx.StaticText(panel, label="C:", pos=(320, 17))
        self.CoolIndicator = LedIndicator.LED(panel, pos=(340, 17), colors=[wx.Colour(220, 10, 10), wx.Colour(10, 220, 10)])
        wx.StaticText(panel, label="F:", pos=(320, 40))
        self.FanIndicator = LedIndicator.LED(panel, pos=(340, 40), colors=[wx.Colour(220, 10, 10), wx.Colour(10, 220, 10)])
        wx.StaticText(panel, label="H:", pos=(370, 17))
        self.HeatIndicator = LedIndicator.LED(panel, pos=(390, 17), colors=[wx.Colour(220, 10, 10), wx.Colour(10, 220, 10)])


        # Thermostat LED
        ledColors = [wx.Colour(0,0,0), wx.Colour(0,0,255), wx.Colour(0,255,0), wx.Colour(0,255,255),
                     wx.Colour(255,0,0), wx.Colour(255,0,255), wx.Colour(255,255,0), wx.Colour(255,255,255)]
        self.ThermostatLed = LedIndicator.LED(panel, pos=(10, 70), colors=ledColors)

        # LCD display
        self.Display = wx.TextCtrl(parent=panel, id=-1, pos=(38, 70), size=(410, 90), style=wx.TE_MULTILINE|wx.TE_READONLY|wx.TE_AUTO_URL)

        self.Display.SetFont(wx.Font(26, wx.FONTFAMILY_MODERN, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
        #self.Display.SetFont(wx.Font(10, wx.MODERN, wx.NORMAL, wx.NORMAL, False, u'Consolas'))

        # set up event callbacks
        self.Bind(wx.EVT_BUTTON, self.onLeft, self.LeftButton)
        self.Bind(wx.EVT_BUTTON, self.onSelect, self.SelectButton)
        self.Bind(wx.EVT_BUTTON, self.onRight, self.RightButton)
        self.Bind(wx.EVT_BUTTON, self.onHome, self.HomeButton)
        self.Bind(wx.EVT_BUTTON, self.onBack, self.BackButton)

        self.Bind(wx.EVT_SPINCTRL, self.OnCurrentTempChange, self.CurrentTemp) 

class TestApp(wx.App):
    def OnInit(self):
        self.TestFrame = TestFrame()
        self.TestFrame.Show()
        self.SetTopWindow(self.TestFrame)
        return True

def main():
    App = TestApp(redirect = False)
    App.MainLoop()

if __name__ == "__main__":
    main()
