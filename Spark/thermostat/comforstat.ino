// This #include statement was automatically added by the Particle IDE.
//#include "Serial_LCD_SparkFun/Serial_LCD_SparkFun.h"
#include "Serial_LCD_SparkFun.h"



// Initialize the LCD library
Serial_LCD_SparkFun lcd;

void setup() {

 delay (3000);

 // Start with a blank slate
 lcd.clear();
 lcd.display();
 lcd.setBrightness(30);
}

// Don't need to do anything in the loop
void loop() {
	static int someNumber = 0;
	someNumber++;

	// Always best to clear the display first
	lcd.clear();

	// We have to print to Serial1
	Serial1.print(someNumber);
	Serial.print(someNumber);

	//lcd.display();

	// Delay 500 milliseconds so it's not too fast
	delay(500);
}

