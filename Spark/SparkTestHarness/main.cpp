int RedLED         = D4;  //SPARK D4 ARDUINO 6
int BlueLED        = D2;  //SPARK D2 ARDUINO 4
int GreenLED       = D3;  //SPARK D3 ARDUINO 5
int UB             = A0;  //SPARK A0 ARDUINO A7
int DB             = A1;  //SPARK A1 ARDUINO A6
int EncoderSwitch  = D0;  //SPARK D0 ARDUINO 2
int EncoderRotateA = D1;  //SPARK D1 ARDUINO 3
int EncoderRotateB = D5;  //SPARK D5 ARDUINO 7


/*
int HS             =  D6;  //SPARK D6 ARDUINO 8
int HR             =  D7;  //SPARK D7 ARDUINO 9
int CS             = A3;  //SPARK A3 ARDUINO A4
int CR             = A2;  //SPARK A2 ARDUINO A5
int FS             = A4;  //SPARK A4 ARDUINO A3
int FR             = A5;  //SPARK A5 ARDUINO A2
int TO             = A6;  //SPARK A6 ARDUINO A1
int DT             = A7;  //SPARK A7 ARDUINO A0
*/

int x = 0;
int xlast = 0;
//int RelayDelay = 20;


void setup() {

    pinMode(RedLED,   OUTPUT);
	pinMode(BlueLED,  OUTPUT);
	pinMode(GreenLED, OUTPUT);
    pinMode(EncoderSwitch,  INPUT);
    pinMode(EncoderRotateA, INPUT);
	pinMode(EncoderRotateB, INPUT);
	pinMode(UB, INPUT);
	pinMode(DB, INPUT);

	
	/*
	pinMode(HR, OUTPUT);
	pinMode(HS, OUTPUT);
	pinMode(CR, OUTPUT);
	pinMode(CS, OUTPUT);
	pinMode(FR, OUTPUT);
	pinMode(FS, OUTPUT);
    */
    
    x = 0;
    xlast = 1;

    Serial1.begin(9600); //spark core turn on serial to display
    delay(1000);
    Serial.write(124);
    Serial.write(140);
    Serial.write(254);
    Serial.write(12);
    
}

void loop() {

    digitalWrite(BlueLED,  !digitalRead(EncoderSwitch));
    digitalWrite(GreenLED, digitalRead(EncoderRotateA));
    digitalWrite(RedLED,   digitalRead(EncoderRotateB));
    
 
    
    if (digitalRead(UB) == HIGH)

	{

		if (x <= 2)
		{
			x = x + 1;
		}
		else
		{
			x = 0;
		}
		delay (300);
	}


	if (digitalRead(DB) == HIGH)

	{

		if (x >= 1)
		{
			x = x - 1;
		}
		else
		{
			x = 3;
		}
		delay (300);
	}


	if (x == 1 && xlast!=x)
	{
        Serial1.write(254);
        Serial1.write(1);
        Serial1.write(254);
        Serial1.write(128);
        Serial1.print("HEAT");
        xlast = x;
        
		/*
		digitalWrite(HS, HIGH);
		delay(RelayDelay);
		digitalWrite(HS, LOW);
	    */
		
	}




	else if (x == 2 && xlast!=x)
	{
	    Serial1.write(254);
        Serial1.write(1);
        Serial1.write(254);
        Serial1.write(128);
        Serial1.print("COOL");
        xlast = x;
		
		
		/*
		digitalWrite(HR, HIGH);
		delay(RelayDelay);
		digitalWrite(HR, LOW);
		digitalWrite(CS, HIGH);
		delay(RelayDelay);
		digitalWrite(CS, LOW);
		*/
	}

	else if (x == 3 && xlast!=x)
	{
        Serial1.write(254);
        Serial1.write(1);
        Serial1.write(254);
        Serial1.write(128);
        Serial1.print("FAN");
        xlast = x;
		
	
		/*
		digitalWrite(CR, HIGH);
		delay(RelayDelay);
		digitalWrite(CR, LOW);
		digitalWrite(FS, HIGH);
		delay(RelayDelay);
		digitalWrite(FS, LOW);
		*/
	}

    else if (x == 0 && xlast!=x)
	{
        Serial1.write(254);
        Serial1.write(1);
        Serial1.write(254);
        Serial1.write(128);
        Serial1.print("OFF");
        xlast = x;
		
	
		
		/*
		digitalWrite(HR, HIGH);
		delay(RelayDelay);
		digitalWrite(HR, LOW);
        
        digitalWrite(CR, HIGH);
		delay(RelayDelay);
		digitalWrite(CR, LOW);

		digitalWrite(FR, HIGH);
		delay(RelayDelay);
		digitalWrite(FR, LOW);
		*/
	    
	}




}