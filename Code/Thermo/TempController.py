#!/bin/env python

import led
import threading
import time

import Environment as env
import Config as cfg

kModes = ["Off", "Cool", "Heat", "Fan"]

class TempController(object):
    def __init__(self):
        self.thread = None
        self.timeOn = { "Off":0, "Cool":0, "Heat":0 }
        self.timeOff = { "Off":0, "Cool":0, "Heat":0 }
        self.actualMode = "Off" # the mode the relays are actually exhibiting
        self.prevMode = "Off"   # the previous mode we last transitioned from
        self._mode = "Off"      # the mode we are trying to go to (could be actual mode as well)
        self._heatRelay = False
        self._coolRelay = False
        self._fanRelay = False

        # setup min on/off times based on configs
        self.minOffTimes = { "Cool":cfg.CoolMinOff, "Heat":cfg.HeatMinOff, "Fan":cfg.FanMinOff }
        self.minOnTimes  = { "Cool":cfg.CoolMinOn,  "Heat":cfg.HeatMinOn,  "Fan":cfg.FanMinOn }

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        if not value in kModes:
            raise Exception("Mode %s not supported"%value)

        # only take actions if trying to change the modes
        if value != self._mode:
            self._mode = value
            self.transitionHandler()


    def transitionHandler(self):
        """
        Te transitionHandler function handles the transitions between temp
        modes: Off, Cool, Heat, and Fan. This also does the logic to manage
        min on/off times of each of the modes.

        Note: It is required to call this function on a periodic basis so that
              the transition can happen at a later time if the min on/off times
              have not been met on the initial transition.
        """
        # don't do anything if we don't need to
        if self._mode == self.actualMode:
            return

        # check transitions ... if we can transition, then setup
        # all the relays and LEDs accordingly. Also need to make
        # note of the time things turned on/off so we can do min
        # on/off times when making the next transition
        if self._mode in "Off" and self.canTransition():
            self.heatRelay = False
            self.coolRelay = False
            self.fanRelay = False

            led.red.isOn = False
            led.green.isOn = False
            led.blue.isOn = False

            self.timeOff[self.actualMode] = time.time()
            self.prevMode = self.actualMode
            self.actualMode = "Off"
        elif self._mode in "Cool" and self.canTransition():
            self.heatRelay = False
            self.coolRelay = True
            self.fanRelay = True

            led.red.isOn = False
            led.green.isOn = False
            led.blue.isOn = True

            self.timeOn["Cool"] = time.time()
            self.prevMode = self.actualMode
            self.actualMode = "Cool"
        elif self._mode in "Heat" and self.canTransition():
            self.heatRelay = True
            self.coolRelay = False
            self.fanRelay = False

            led.red.isOn = True
            led.green.isOn = False
            led.blue.isOn = False

            self.timeOn["Heat"] = time.time()
            self.prevMode = self.actualMode
            self.actualMode = "Heat"
        elif self._mode in "Fan" and self.canTransition():
            self.heatRelay = False
            self.coolRelay = False
            self.fanRelay = True

            led.red.isOn = False
            led.green.isOn = True
            led.blue.isOn = False

            self.timeOn["Fan"] = time.time()
            self.prevMode = self.actualMode
            self.actualMode = "Fan"

    def canTransition(self):
        """
        The canTransition function evaluates the min on/off time requirements
        to see if we are allowed to make the transition we are attempting to
        make.

        Returns:
          bool: True if a transition is allowed, False otherwise
        """

        # this sould be protected elsewhere, but we'll go ahead and do
        # another sanity check to make sure we aren't trying to transition
        # within the same modes
        if self._mode == self.actualMode:
            return True

        if self._mode == "Off":
            # if trying to turn Heat/Cool/Fan off ... then need to validate
            # min on times are met before allowing it
            timeOn = self.timeOn[self.actualMode]
            timeNow = time.time()
            minOnTime = self.minOnTimes[self.actualMode]

            print "actualMode:", self.actualMode, "timeOn:", timeOn, "timeNow:", timeNow, "minOnTime:", minOnTime

            if (timeNow - timeOn) < (minOnTime * 60):
                # we haven't met the min on time yet, don't allow transition
                return False
            else:
                # min on time has been met ... go ahead and transition
                return True
        else:
            # TODO: Figure out wtf this is trying to accomplish :-/
            if self.prevMode == "Off":
                return True

            # get the time off times for whatever mode was previously on
            timeOff = self.timeOff[self.prevMode]
            timeNow = time.time()
            minOffTime = self.minOffTimes[self.prevMode]

            print "actualMode:", self.actualMode, "timeOff:", timeOff, "timeNow:", timeNow, "minOffTime:", minOffTime

            if (timeNow - timeOff) < (minOffTime * 60):
                # we haven't met the min off time yet, don't allow transition
                return False
            else:
                # min off time has been met ... go ahead and transition
                return True

    @property
    def heatRelay(self):
        return self._heatRelay

    @heatRelay.setter
    def heatRelay(self, value):
        if type(value) != type(False):
            raise Exception("heatRelay must be a boolean")

        # only change the relay state if it needs to
        if self._heatRelay != value:
            with open(env.HeatRelayPath, 'w') as f:
                f.write("1" if value else "0")

        self._heatRelay = value

    @property
    def coolRelay(self):
        return self._coolRelay

    @coolRelay.setter
    def coolRelay(self, value):
        if type(value) != type(False):
            raise Exception("coolRelay must be a boolean")

        # only change the relay state if it needs to
        if self._coolRelay != value:
            with open(env.CoolRelayPath, 'w') as f:
                f.write("1" if value else "0")

        self._coolRelay = value

    @property
    def fanRelay(self):
        return self._fanRelay

    @fanRelay.setter
    def fanRelay(self, value):
        if type(value) != type(False):
            raise Exception("fanRelay must be a boolean")

        # only change the relay state if it needs to
        if self._fanRelay != value:
            with open(env.FanRelayPath, 'w') as f:
                f.write("1" if value else "0")

        self._fanRelay = value

