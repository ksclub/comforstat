#!/bin/env python
# standard python imports
import time
import datetime
import serial
import sys
import os
import traceback

# third party packages
import pyinotify

# our packages
import TempController
import Environment as env
import Config as cfg

# initialize local variables
targetTemp = 72
currentTemp = 72
curSysMode = "Off"
tempController = TempController.TempController()
handler = None
notifier = None

# This EventHandler class is used for handling events related to file
# change notifications
class EventHandler(pyinotify.ProcessEvent):
    def __init__(self, callback):
        self.callback = callback

    def process_IN_MODIFY(self, event):
        self.callback(event.pathname)

    def process_IN_CREATE(self, event):
        print "Creating:", event.pathname

    def process_IN_DELETE(self, event):
        print "Removing:", event.pathname

def parseCurSysMode():
    global curSysMode

    with open(env.CurSysModePath, 'r') as f:
        value = f.read(100)

    if value != '':
        curSysMode = value.rstrip()

def parseCurrentTemp():
    global currentTemp

    with open(env.CurrentTempPath, 'r') as f:
        value = f.read(100)

    if value != '':
        try:
            currentTemp = float(value)
        except ValueError:
            pass

def parseTargetTemp():
    global targetTemp

    with open(env.TargetTempPath, 'r') as f:
        value = f.read(100)

    if value != '':
        targetTemp = int(value)

def onFileUpdate(iFilePath):
    global ui

    if iFilePath in env.CurSysModePath:
        parseCurSysMode()
    elif iFilePath in env.CurrentTempPath:
        parseCurrentTemp()
    elif iFilePath in env.TargetTempPath:
        parseTargetTemp()

def checkFileUpdates():
    global notifier

    assert notifier._timeout is not None, 'Notifier must be constructed with a short timeout'
    notifier.process_events()
    while notifier.check_events():  #loop in case more events appear while we are processing
        notifier.read_events()
        notifier.process_events()

# initializatin of the UI application
def init():
    global ui, targetTemp, sock, gCurEncoderPos, handler, notifier

    parseCurrentTemp()
    parseTargetTemp()
    parseCurSysMode()

    # setup file change notifications for current temp, current system mode, and temp mode
    wm = pyinotify.WatchManager()
    mask = pyinotify.IN_MODIFY | pyinotify.IN_DELETE | pyinotify.IN_CREATE  # watched events
    wm.add_watch(env.CurSysModePath, mask)
    wm.add_watch(env.CurrentTempPath, mask)
    wm.add_watch(env.TargetTempPath, mask)

    handler = EventHandler(onFileUpdate)
    notifier = pyinotify.Notifier(wm, handler, timeout=10)

# the "main loop"
def run():
    global curSysMode, currentTemp, targetTemp

    tempCheckTimer = time.time() + 1

    # initialize timer for updating the time on the display
    timeTimer = time.time() + 10

    while True:
        time.sleep(0.1)

        # if the timer has expired, then set the time and reset the timer
        if time.time() > timeTimer:
            timeTimer = time.time() + 10
            #setCurrentTime()
 
        if time.time() > tempCheckTimer:
            print "Mode:", curSysMode, "Cur:", currentTemp, "Tar:", targetTemp

            if curSysMode in "Cool":
                if currentTemp > targetTemp + cfg.CoolHysteresis:
                    tempController.mode = curSysMode
                elif currentTemp < targetTemp - cfg.CoolHysteresis:
                    tempController.mode = "Off"

            elif curSysMode in "Heat":
                if currentTemp < targetTemp - cfg.HeatHysteresis:
                    tempController.mode = curSysMode
                elif currentTemp > targetTemp + cfg.HeatHysteresis:
                    tempController.mode = "Off"

            elif curSysMode in "Auto":
                if currentTemp < targetTemp - cfg.AutoHysteresis:
                    tempController.mode = "Heat"
                elif currentTemp > targetTemp + cfg.AutoHysteresis:
                    tempController.mode = "Cool"
                else:
                    tempController.mode = "Off"

            tempController.transitionHandler()
    
            tempCheckTimer = time.time() + 1

        # check for file modifications
        checkFileUpdates()

def main():
    init()
    try:
        run()
    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    main()
