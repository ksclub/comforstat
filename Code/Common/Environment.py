#!/bin/evn python

import os

BasePath = "/apps/interfaces/"

# get file paths from environment

#    physical inputs
EncoderKnobPath = os.getenv('ENCODER_KNOB_PATH', BasePath + "encoderKnob")
EncoderPushPath = os.getenv('ENCODER_PUSH_PATH', BasePath + "encoderPush")
HomeButtonPath  = os.getenv('HOME_BUTTON_PATH', BasePath + "homeButton")
BackButtonPath  = os.getenv('BACK_BUTTON_PATH', BasePath + "backButton")
UartPath        = os.getenv('UART_PATH', BasePath + "uart")
DigThermPath    = os.getenv('DIGITAL_THERMOMETER_PATH', BasePath + "digTherm")
AnalogThermPath = os.getenv('ANALOG_THERMOMETER_PATH', BasePath + "analogTherm")

#    physical outputs
LedRedPath    = os.getenv('LED_RED_PATH', BasePath + "ledRed")
LedBluePath   = os.getenv('LED_BLUE_PATH', BasePath + "ledBlue")
LedGreenPath  = os.getenv('LED_GREEN_PATH', BasePath + "ledGreen")
HeatRelayPath = os.getenv('HEAT_RELAY_PATH', BasePath + "heatRelay")
CoolRelayPath = os.getenv('COOL_RELAY_PATH', BasePath + "coolRelay")
FanRelayPath  = os.getenv('FAN_RELAY_PATH', BasePath + "fanRelay")
AuxRelayPath  = os.getenv('AUX_RELAY_PATH', BasePath + "auxRelay")

#    inter process communication
CurrentTempPath = os.getenv('CURRENT_TEMP_PATH', BasePath + "currentTemp")
TargetTempPath  = os.getenv('TARGET_TEMP_PATH', BasePath + "targetTemp")
ProgModePath    = os.getenv('PROG_MODE_PATH', BasePath + "progMode")
CurSysModePath  = os.getenv('CURRENT_SYSTEM_MODE_PATH', BasePath + "curSysMode")
FanModePath     = os.getenv('FAN_MODE_PATH', BasePath + "fanMode")

#    schedule locations
ScheduleFolderPath = os.getenv('SCHEDULE_FOLDER_PATH', BasePath + "schedules/")
ActiveSchedulePath = os.getenv('ACTIVE_SCHEDULE_PATH', BasePath + "activeSchedule")

#    config parameters
ClockFormatPath = os.getenv('CLOCK_FORMAT_PATH', BasePath + "clockFormat")
TempFormatPath  = os.getenv('TEMP_FORMAT_PATH', BasePath + "tempFormat")

