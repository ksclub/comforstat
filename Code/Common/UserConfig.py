#!/bin/env python

import sys

class _UserConfig(object):
    def __init__(self):
        import Environment
        self.env = Environment

    @property
    def tempFormat(self):
        try:
            with open(self.env.TempFormatPath) as f:
                tempFormat = f.read()
        except:
            tempFormat = "F"
            print "Failed to open temp format file (", self.env.TempFormatPath, ")"

        return tempFormat

    @tempFormat.setter
    def tempFormat(self, newTempFormat):
        if newTempFormat not in ["C", "F"]:
            raise Exception("tempFormat must be 'F' or 'C'")

        with open(self.env.TempFormatPath, "w") as f:
            f.write(newTempFormat)

    @property
    def clockFormat(self):
        try:
            with open(self.env.ClockFormatPath) as f:
                clockFormat = f.read()
        except:
            clockFormat = "F"
            print "Failed to open clock format file (", self.env.ClockFormatPath, ")"

        return clockFormat

    @clockFormat.setter
    def clockFormat(self, newClockFormat):
        if newClockFormat not in ["C", "F"]:
            raise Exception("clockFormat must be 'F' or 'C'")

        with open(self.env.ClockFormatPath, "w") as f:
            f.write(newClockFormat)

sys.modules[__name__] = _UserConfig()

