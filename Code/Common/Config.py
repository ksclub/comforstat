#!/bin/env python

# The values within this file are "configurable" in the sense that they
# can be changed here to modify the behaviour within the system. They are
# not configurable from the GUI on the thermostate nor through the web


# hysteresis values to keep from kicking on too often
HeatHysteresis = 1 # hysteresis (in degrees F) for maintaining temp in heat mode
CoolHysteresis = 1 # hysteresis (in degrees F) for maintaining temp cool mode
AutoHysteresis = 1 # hysteresis (in degrees F) for maintaining temp in auto mode

# min values for things to be on/off
HeatMinOn  = 5    # Minimum time (in minutes) the heat has to be on before shutting off
HeatMinOff = 10   # Minimum time (in minutes) the heat has to be off before turning on
CoolMinOn  = 5    # Minimum time (in minutes) the cool has to be on before shutting off
CoolMinOff = 10   # Minimum time (in minutes) the cool has to be off before turning on
FanMinOn  = 0     # Minimum time (in minutes) the fan has to be on before shutting off
FanMinOff = 0     # Minimum time (in minutes) the fan has to be off before turning on

# TODO: This is not implemented
# lead/lag for fan with heat/cool
HeatFanLag  = 0   # How long (in minutes) the fan will stay on after heat is turned off
HeatFanLead = 0   # How long (in minutes) the fan will turn on before heat is turned on
CoolFanLag  = 0   # How long (in minutes) the fan will stay on after cool is turned off
CoolFanLead = 0   # How long (in minutes) the fan will turn on before cool is turned on

# time windows for programs

# misc
HeatFanOnWith = False # should the fan be turned on when heat is on
CoolFanOnwith = True  # should the fan be turned on when cool is on

# encoder stuff
EncoderDivider = 4

# temp filtering
TempReadPeriod  = 5     # number of seconds between each read
TempPrevWeight  = 0.95  # weight of the previously read values (this and cur weight should add to 1)
TempCurWeight   = 0.05  # weight of the currently read values (this and prev weight should add to 1)
TempFudgeFactor = -10.0   # this is a fudge factor (in deg F) used to get temp to read proper value
