#!/bin/bash

usage()
{
    if [[ "$1" != "" ]]; then
        echo "FAILED: $1"
    fi

    echo "Usage: $0 -d <drive partition> [-s <wifi ssid>] [-w <wpa key>]"
    exit 0
}

fail()
{
    if [[ "$1" != "" ]]; then
        echo "FAILED: $1"
    fi
    exit 0
}

while [ "$1" != "" ]; do
    if [[ "$1" == "-d" ]]; then
        shift
        drivePart=$1
        shift
    elif [[ "$1" == "-s" ]]; then
        shift
        ssid=$1
        shift
    elif [[ "$1" == "-w" ]]; then
        shift
        wpakey=$1
        shift
    else
        echo "Unrecognized command $1"
        usage
    fi
done

[ "$drivePart" == "" ] && usage "drive partition must be specified"

[ ! -e "$drivePart" ] && fail "$drivePart doesn't exist"

installDir="/opt/comforstat/"
pythonLocalInstallDir="$installDir/python/"
scriptLocalInstallDir="$installDir/bin/"
initTabContents="::respawn:${scriptLocalInstallDir}/runOneWireTemp.sh\n::respawn:${scriptLocalInstallDir}/runScheduler.sh\n::respawn:${scriptLocalInstallDir}/runThermo.sh\n::respawn:${scriptLocalInstallDir}/runUI.sh\n::respawn:${scriptLocalInstallDir}/runStats.sh\n"

mountLocation="/mnt/sdcard2/"
pythonInstallDir="$mountLocation/$pythonLocalInstallDir"
scriptInstallDir="$mountLocation/$scriptLocalInstallDir"

if [ "$pythonInstallDir" = "/opt/comforstat/" ]; then
    fail "Something went wrong"
fi
if [ "$scriptInstallDir" = "/usr/local/bin/" ]; then
    fail "Something went wrong"
fi

umount $drivePart
install -d $mountLocation

# mount as read/write
mount $drivePart $mountLocation

# install our python files
install -d ${pythonInstallDir}
cp -r Statistics Common UI Thermo Schedule ${pythonInstallDir}
install -d ${pythonInstallDir}/HwInterface
cp HwInterface/*.py ${pythonInstallDir}/HwInterface

# install file system overlay
cp -r fs_overlay/* $mountLocation/.

# setup init.d script
rm -f ${mountLocation}/etc/init.d/S51comforstat
ln -s ${installDir}/init.d/rcS ${mountLocation}/etc/init.d/S51comforstat

# create respawn tasks in /etc/inittab for all our apps
grep -q "runOneWireTemp" $mountLocation/etc/inittab

if [ $? -ne 0 ]; then
    sed -i "s#\(/etc/init.d/rcS\)#\1\n${initTabContents}#" $mountLocation/etc/inittab
fi

if [[ "$ssid" != "" ]]; then
    sed -i "s/ssid=\".*\"/ssid=\"${ssid}\"/" $mountLocation/etc/wpa_supplicant.conf
    sed -i 's/#udhcpc/udhcpc/' $mountLocation/etc/init.d/S42wpa_supplicant
fi

if [[ "$wpakey" != "" ]]; then
    sed -i "s/psk=\".*\"/psk=\"${wpakey}\"/" $mountLocation/etc/wpa_supplicant.conf
    sed -i 's/#udhcpc/udhcpc/' $mountLocation/etc/init.d/S42wpa_supplicant
fi

# clean up
sync
umount $mountLocation
[ -d "$mountLocation" ] && rm -rf $mountLocation

