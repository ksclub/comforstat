#!/bin/evn python

import UIEvents

class Choice(object):
    def __init__(self, iDisplay, iTitle, iChoices):
        self.display = iDisplay
        self.title = iTitle
        self.choices = iChoices
        self.choiceStartPos = {}

        startPos = 0
        for choice in self.choices:
            self.choiceStartPos[choice] = startPos
            startPos += len(choice) + 1

        self.currentIndex = 0

    def setChoices(self, iChoices):
        print "Setting choices"
        self.choices = iChoices
        self.choiceStartPos = {}

        startPos = 0
        for choice in self.choices:
            self.choiceStartPos[choice] = startPos
            startPos += len(choice) + 1

        self.currentIndex = 0

    def onBack(self):
        return UIEvents.BACK

    def onSelect(self):
        return self.choices[self.currentIndex]

    def nextItem(self):
        self.currentIndex += 1

        if self.currentIndex >= len(self.choices):
            self.currentIndex = 0
        
        self.draw()

    def prevItem(self):
        self.currentIndex -= 1

        if self.currentIndex < 0:
            self.currentIndex = len(self.choices) - 1
        
        self.draw()

    # default action is to just return the string of the choice
    def action(self, choice):
        return choice

    def event(self, iEvent):
        if iEvent == UIEvents.CCW:
            # we want the menu items on the screen to follow the same direction
            # as the dial is turning. Because the items are displayed like this:
            #
            #           --------------------------------
            #           |         Current Item         |
            #           | Prev Item          Next Item |
            #           --------------------------------
            #
            # then a counter clockwise movement on the dial should move to the
            # next item so that the item on the right move up into the current
            # item slot. And a clockwise movement should move to the previous
            # item in the list so that item moves up into the current item slot
            self.nextItem()
        elif iEvent == UIEvents.CW:
            # see above comment to see why we go to prev item here
            self.prevItem()
        elif iEvent == UIEvents.MENU:
            pass # Menu button does nothing here for now
        elif iEvent == UIEvents.BACK:
            return UIEvents.BACK
        elif iEvent == UIEvents.SELECT:
            return self.action(self.choices[self.currentIndex])

        return None

    def draw(self):
        self.display.clear()
       
        prev = self.choices[self.currentIndex - 1]
        current = self.choices[self.currentIndex]
        next = self.choices[(self.currentIndex + 1) % len(self.choices)]
        #print "prev:", prev, "current:", current, "next:", next

        prevSize = len(prev)
        currentSize = len(current)
        nextSize = len(next)
        #print "size ... prev:", prevSize, "current:", currentSize, "next:", nextSize

        self.display.writeText(current, int((16 - currentSize)/2), 0)
        self.display.writeText(prev, 0, 1)
        self.display.writeText(next, 16 - nextSize, 1)

        return True

    def show(self):
        self.draw()


