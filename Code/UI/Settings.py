#!/bin/evn python
import json

import UIEvents
import Menu
import Environment as env

import ClockFormatChoice
import TempFormatChoice


class Settings(object):
    def __init__(self, iDisplay):
        self.display = iDisplay
        self.menu = Menu.Menu(self.display, {"Clock": ClockFormatChoice.ClockFormatChoice(self.display, "Clock Format"),
                                             "Temp": TempFormatChoice.TempFormatChoice(self.display, "Temp Format"),
                                             "WiFi": 0,
                                             "Time": 0 })

    def event(self, iEvent):
        ret = self.menu.event(iEvent)
        if ret == UIEvents.BACK:
            return UIEvents.BACK

        return None

    def show(self):
        self.display.clear()

        self.menu.show()
        
        return True

