#!/bin/env python
import datetime

import Environment as env
import UserConfig as usrCfg
import LCD
#import FakeLCD as LCD
import Menu
import UIEvents

# GUI elements
import PrgChoice
import SysChoice
import FanChoice
import ScheduleEditor
import Settings

kProgModes = ["Prg", "Hld", "Prg-Hld"]

class UserInterface(object):
    def __init__(self, iPort=None):
        self.display = LCD.LCD(iPort)
        self.display.clear()

        self._currentTime = 100
        self._targetTemp = None
        self._currentTemp = None

        self.menu = Menu.Menu(self.display, {"Prg/hld": PrgChoice.PrgChoice(self.display, "Prg/Hld"),
                                             "Settings": Settings.Settings(self.display),
                                             "System": SysChoice.SysChoice(self.display, "System"),
                                             "Schedule": ScheduleEditor.ScheduleEditor(self.display),
                                             "Fan": FanChoice.FanChoice(self.display, "Fan") })
        self._menuShown = False

    @property
    def menuShown(self):
        return self._menuShown

    @menuShown.setter
    def menuShown(self, value):
        # if the menu is currently shown and we are transititioning to being not shown,
        # then we need to clear the display for whatever is being shown next
        if self._menuShown and not value:
            self.display.clear()

        self._menuShown = value

    @property
    def targetTemp(self):
        return self._targetTemp

    @targetTemp.setter
    def targetTemp(self, value):
        self._targetTemp = value
        if self._targetTemp and not self.menuShown:
            tempFormat = usrCfg.tempFormat
            temp = self._targetTemp

            if tempFormat == "C":
                temp = (temp - 32) * 5 / 9

            self.display.writeText("%-2d%s"%(temp, tempFormat), 0, 1)

    @property
    def currentTemp(self):
        return self._currentTemp

    @currentTemp.setter
    def currentTemp(self, value):
        self._currentTemp = value
        if self._currentTemp and not self.menuShown:
            tempFormat = usrCfg.tempFormat
            temp = self._currentTemp

            if tempFormat == "C":
                temp = (temp - 32) * 5 / 9

            self.display.writeText("%-2d%s"%(temp, tempFormat), 0, 0)

    @property
    def currentTime(self):
        return self._currentTime

    @currentTime.setter
    def time(self, value):
        self._currentTime = value
        if self._currentTime and not self.menuShown:
            # current time is always reported in 24 hour clock format, so this
            # will get us the proper format for a 24 hour clock
            timeString = "%d:%02d"%(self._currentTime.hour, self._currentTime.minute)

            # if the user has configured a 12 hour clock we need to reformat
            # our time from the 24 hour clock format
            if usrCfg.clockFormat == "12":
                d = datetime.datetime.strptime(timeString, "%H:%M")
                timeString = d.strftime("%I:%M %p")

            # display this in the upper right corner
            #    - the (16 - string length) gives us how far left the string
            #      needs to be so that it ends on the right edge of the display
            self.display.writeText(timeString, 16 - len(timeString), 0)

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        if not value in kProgModes:
            raise Exception("Mode \"%s\" not recognized"%value)
        self._mode = value
        if self._mode and not self.menuShown:
            # There are only really two modes "Prg" and "Hld", but we need more detail
            # for some other aspects of the thermostat so the convention is to use
            # Prg-<detail> or Hld-<detail> to define the extra detail. Plus our display
            # layout only allows for 3 characters, so just hack off the first 3 characters
            # and throw them at the display
            self.display.writeText(self._mode[:3], 13, 1)

    def clear(self):
        self.display.clear()

    def onCW(self):
        if self.menuShown:
            self.menu.event(UIEvents.CW)

    def onCCW(self):
        if self.menuShown:
            self.menu.event(UIEvents.CCW)

    def onMenu(self):
        if self.menuShown:
            print "Select Pressed"
            self.menu.event(UIEvents.SELECT)
        else:
            print "Show Menu"
            self.menuShown = self.menu.show()

    def onBack(self):
        if self.menuShown:
            print "Back Pressed"
            ret = self.menu.event(UIEvents.BACK)

            if ret == UIEvents.BACK:
                self.menuShown = False
                return UIEvents.BACK

    def blinkCycle(self):
        self.display.blinkCycle()


