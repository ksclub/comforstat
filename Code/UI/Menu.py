#!/bin/env python
import UIObject
import UIEvents

class Menu(UIObject.UIObject):
    def __init__(self, iDisplay, iItems):
        self.display = iDisplay
        self.itemMap = iItems
        self.itemNames = iItems.keys()
        self.currentItem = 0
        self.iAmDisplayed = False

    def onSelect(self):
        pass

    def nextItem(self):
        self.currentItem += 1

        if self.currentItem >= len(self.itemNames):
            self.currentItem = 0
        
        self.show()

    def prevItem(self):
        self.currentItem -= 1

        if self.currentItem < 0:
            self.currentItem = len(self.itemNames) - 1
        
        self.show()

    def event(self, iEvent):
        if self.iAmDisplayed:
            if iEvent == UIEvents.CCW:
                self.nextItem() # On CCW we actually want the next item (bottom right) to go to the top
            elif iEvent == UIEvents.CW:
                self.prevItem() # On CW we actually want the previous item (bottom left) to go to the top
            elif iEvent == UIEvents.MENU:
                pass # Menu button does nothing here for now
            elif iEvent == UIEvents.BACK:
                return UIEvents.BACK
            elif iEvent == UIEvents.SELECT:
                self.itemMap[self.itemNames[self.currentItem]].show()
                self.iAmDisplayed = False
                pass # TODO: The select button should do stuff too
        else:
            ret = self.itemMap[self.itemNames[self.currentItem]].event(iEvent)

            if ret == UIEvents.BACK:
                self.show()

    def show(self):
        self.display.clear()
        
        prev = self.itemNames[self.currentItem - 1]
        current = self.itemNames[self.currentItem]
        next = self.itemNames[(self.currentItem + 1) % len(self.itemNames)]

        prevSize = len(prev)
        currentSize = len(current)
        nextSize = len(next)

        self.display.writeText(current, int((16 - currentSize)/2), 0)
        self.display.writeText(prev, 0, 1)
        self.display.writeText(next, 16 - nextSize, 1)

        self.iAmDisplayed = True

        return True

