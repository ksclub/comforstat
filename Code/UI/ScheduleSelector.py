#!/bin/evn python

import os

import UIEvents
import Choice

class ScheduleSelector(Choice.Choice):
    def __init__(self, iDisplay, iSchedulePath):
        self.display = iDisplay
        self.schedulePath = iSchedulePath

        super(ScheduleSelector, self).__init__(iDisplay, "Schedules", self.getSchedules())

    def getSchedules(self):
        schedules = ['heat', 'cool', 'auto']
        return schedules

#TODO: Make schedule editing a bit more complex by allowing to edit any schedule
#        schedules = []
#        for (_, _, filenames) in os.walk(self.schedulePath):
#            schedules = filenames
#            break # we don't handle sub folders so just break out of here
#
#        schedules = [ x[:-5] for x in schedules ]
#        return schedules

    def show(self):
        self.setChoices(self.getSchedules())
        super(ScheduleSelector, self).show()

