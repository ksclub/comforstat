#!/bin/env python

import Choice
import Environment as env
import UIEvents

class SysChoice(Choice.Choice):
    def __init__(self, iDisplay, iTitle):
        super(SysChoice, self).__init__(iDisplay, iTitle, ["Heat", "Cool", "Auto", "Off"])

    def action(self, iChoice):
        with open(env.CurSysModePath, 'w') as f:
            f.write(iChoice)

        # TODO: when we get more fancy and allow multiple schedules for heat/cool/auto,
        #       then we need to remove this and add in some sort of "lastHeatSchedule"
        #       thing so that each mode can have their own schedule
        with open(env.ActiveSchedulePath, 'w') as f:
            f.write(iChoice.lower() + ".json")

        return UIEvents.BACK

