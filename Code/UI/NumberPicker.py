#!/bin/evn python

import UIEvents

class NumberPicker(object):
    def __init__(self, iDisplay, iTitle, iMin=0, iMax=99, iStart=None):
        self.display = iDisplay
        self.title = iTitle
        self.min = iMin
        self.max = iMax
        if iStart is not None:
            self.current = iStart
        else:
            self.current = self.min + ((self.max - self.min) / 2)

    def onBack(self):
        return None

    def onSelect(self):
        return self.current

    def nextItem(self):
        if self.current < self.max:
            self.current += 1
        
        self.draw()

    def prevItem(self):
        if self.current > self.min:
            self.current -= 1
        
        self.draw()

    def event(self, iEvent):
        if iEvent == UIEvents.CCW:
            self.prevItem() # On CCW go to previous item
        elif iEvent == UIEvents.CW:
            self.nextItem() # On CW go to next item
        elif iEvent == UIEvents.MENU:
            pass # Menu button does nothing here for now
        elif iEvent == UIEvents.BACK:
            return UIEvents.BACK
        elif iEvent == UIEvents.SELECT:
            return self.onSelect()

        return None

    def draw(self):
        self.display.writeText(self.title, 0, 0)
        self.display.writeText(str(self.current), 0, 1)

        return True

    def show(self):
        self.display.clear()
        self.draw()


