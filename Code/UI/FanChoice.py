#!/bin/env python

import Choice
import UIEvents
import Environment as env

class FanChoice(Choice.Choice):
    def __init__(self, iDisplay, iTitle):
        super(FanChoice, self).__init__(iDisplay, iTitle, ["On", "Auto"])

    def action(self, iChoice):
        with open(env.FanModePath, 'w') as f:
            f.write(iChoice)
        
        return UIEvents.BACK

