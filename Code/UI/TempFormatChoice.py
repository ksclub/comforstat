#!/bin/env python

import Choice
import UIEvents
import Environment as env

class TempFormatChoice(Choice.Choice):
    def __init__(self, iDisplay, iTitle):
        super(TempFormatChoice, self).__init__(iDisplay, iTitle, ["F", "C"])

    def action(self, iChoice):
        with open(env.TempFormatPath, 'w') as f:
            f.write(iChoice)
        
        return UIEvents.BACK

