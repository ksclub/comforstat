#!/bin/evn python
import json

import UIEvents
import Choice
import ScheduleSelector
import NumberPicker
import Environment as env

kActions = ["Edit", "View", "Choose"]
kSchedules = ["heat", "cool", "auto"]
kDayGroup = ["SatSun", "MoTuWeTh", "Fri", "Done"]
kDayGroupMap = {"SatSun":["saturday", "sunday"],
                "MoTuWeTh":["monday","tuesday","wednesday","thursday"],
                "Fri":["friday"],
                "Done":None }
kHourGroup = ["00-08", "08-17", "17-21", "21-24"]
kHourGroupMap = {"00-08":"00:00",
                 "08-17":"08:00",
                 "17-21":"17:00", 
                 "21-24":"21:00"}
kValidHourStarts = ["00:00", "08:00", "17:00", "21:00"]

class ScheduleEditor(object):
    def __init__(self, iDisplay):
        self.display = iDisplay
        self.state = None
        self.visibleThing = None
        self.actionChoice = Choice.Choice(self.display, "Action", kActions)
        self.scheduleSelector = Choice.Choice(self.display, "Schedule", kSchedules)
        self.dayGroupSelector = Choice.Choice(self.display, "DayGroup", kDayGroup)
        self.hourGroupSelector = Choice.Choice(self.display, "HourGroup", kHourGroup)
        self.tempPicker = NumberPicker.NumberPicker(self.display, "Temp")

        self.nextAction = None
        self.schedule = None
        self.dayGroup = None
        self.hourGroup = None
        self.temp = None

    def goTo_SelectAction(self):
        self.actionChoice.show()
        self.visibleThing = self.actionChoice
        self.state = "SelectAction"
        self.nextAction = None

    def goTo_SelectSchedule(self):
        self.scheduleSelector.show()
        self.visibleThing = self.scheduleSelector
        self.state = "SelectSchedule"
        self.schedule = None

    def goTo_EditSchedule(self):
        self.goTo_PickDayGroup()
        self.dayGroup = None
        self.hourGroup = None
        self.temp = None

    def goTo_PickDayGroup(self):
        self.dayGroupSelector.show()
        self.visibleThing = self.dayGroupSelector
        self.state = "EditSchedule-PickDayGroup"
        self.dayGroup = None

    def goTo_PickHourGroup(self):
        self.hourGroupSelector.show()
        self.visible = self.hourGroupSelector
        self.state = "EditSchedule-PickHourGroup"
        self.hourGroup = None

    def goTo_PickTemp(self):
        self.tempPicker.show()
        self.visible = self.tempPicker
        self.state = "EditSchedule-PickTemp"
        self.temp = None

    def event(self, iEvent):
        if self.state == "SelectAction":
            ret = self.actionChoice.event(iEvent)
            if ret == UIEvents.BACK:
                self.visibleThing = None
                return UIEvents.BACK
            elif ret in kActions:
                self.nextAction = ret

                if ret in [ "Edit", "View", "Choose" ]:
                    self.goTo_SelectSchedule()
                else:
                    pass
        
        elif self.state == "SelectSchedule":
            ret = self.scheduleSelector.event(iEvent)
            if ret == UIEvents.BACK:
                self.goTo_SelectAction()
            elif ret:
                self.schedule = ret
                self.scheduleSelector.show()
                self.visibleThing = self.scheduleSelector
                
                if self.nextAction == "Edit":
                    self.goTo_EditSchedule()
                elif self.nextAction == "View":
                    self.state = "ViewSchedule"
                elif self.nextAction == "Choose":
                    with open(env.ActiveSchedulePath, 'w') as f:
                        f.write(self.schedule + ".json")
                    self.goTo_SelectAction()
        elif self.state == "ViewSchedule":
            pass
        elif self.state.startswith("EditSchedule"):
            if self.state == "EditSchedule" or self.state == "EditSchedule-PickDayGroup":
                ret = self.dayGroupSelector.event(iEvent)
                if ret in kDayGroup:
                    self.dayGroup = ret
                    self.goTo_PickHourGroup()
                elif ret == UIEvents.BACK:
                    self.goTo_SelectAction()
            elif self.state == "EditSchedule-PickHourGroup":
                ret = self.hourGroupSelector.event(iEvent)
                if ret in kHourGroup:
                    self.hourGroup = ret
                    self.goTo_PickTemp()
                elif ret == UIEvents.BACK:
                    self.goTo_PickDayGroup()
                pass
            elif self.state == "EditSchedule-PickTemp":
                ret = self.tempPicker.event(iEvent)
                if ret == UIEvents.BACK:
                    self.goTo_PickHourGroup()
                elif ret is not None:
                    self.temp = ret
                    # TODO: Set value in schedule
                    print "Day:", self.dayGroup, "Hour:", self.hourGroup, "Temp:",  self.temp
                    self.setSchedule(self.schedule,
                                     kDayGroupMap[self.dayGroup],
                                     kHourGroupMap[self.hourGroup],
                                     self.temp)
                    self.goTo_PickDayGroup()
            else:
                print "WTF happened?"
            pass
        else:
            if iEvent == UIEvents.CCW:
                self.visibleThing.event(iEvent)
            elif iEvent == UIEvents.CW:
                self.visibleThing.event(iEvent)
            elif iEvent == UIEvents.MENU:
                pass
            elif iEvent == UIEvents.BACK:
                return UIEvents.BACK
            elif iEvent == UIEvents.SELECT:
                return UIEvents.BACK

        return None

    def setSchedule(self, scheduleName, dayList, startTime, temp):
        for day in dayList:
            self.replaceDay(scheduleName, day, startTime, temp)

    def replaceDay(self, scheduleName, day, startTime, temp):
        with open(env.ScheduleFolderPath + "/" + scheduleName + ".json",) as scheduleFile:
            schedule = json.load(scheduleFile)

        times = []

        print "day:", day
        #print "schedule:", schedule
        print "entries:", schedule['schedule'][day]
        foundTime = False

        for entry in schedule['schedule'][day]:
            if entry['time'] == startTime:
                # this is the time slot we want to update, so do that
                entry['temp'] = temp
                foundTime = True
            elif entry['time'] in kValidHourStarts:
                # keep going and append this since it's a valid time group
                pass
            else:
                # not a valid time group ... just drop this time slot
                continue
            
            times.append(entry)

        if not foundTime:
            times.append({'time':startTime, 'temp':temp})

        print "new entries:", times
        schedule['schedule'][day] = times
        #print "Updating schedule", scheduleName, "with following data:", schedule
                    
        with open(env.ScheduleFolderPath + "/" + scheduleName + ".json","w") as scheduleFile:
            scheduleFile.write(json.dumps(schedule, sort_keys=True, indent=4, separators=(',', ': ')))

    def show(self):
        self.display.clear()

        self.goTo_SelectAction()
        
        return True

