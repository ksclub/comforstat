import UIEvents

class UIObject(object):
    def __init__(self):
        pass

    def event(self, iEvent):
        raise Exception("You need to implement this")

