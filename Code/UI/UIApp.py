#!/bin/env python
# standard python imports
import time
import datetime
import serial
import sys
import os
import traceback

# third party packages
import pyinotify

# our packages
import UserInterface
import UIEvents
import Environment as env
import Config as cfg

# initialize local variables
targetTemp = 72
ui = None
handler = None
notifier = None

# This fakeUart class is used to wrap a Unix pipe in something that looks
# like a normal uart/file I/O interface which has read/write functions
# to get/put data. This is basically only used with the Simulator on host
class fakeUart():
    def __init__(self, iPath):
        self.fd = os.open(iPath, os.O_WRONLY)

    def read(self, iSize):
        return os.read(self.fd, iSize)

    def write(self, iData):
        return os.write(self.fd, iData)

# This EventHandler class is used for handling events related to file
# change notifications
class EventHandler(pyinotify.ProcessEvent):
    def __init__(self, callback):
        self.callback = callback

    def process_IN_MODIFY(self, event):
        self.callback(event.pathname)

    def process_IN_CREATE(self, event):
        print "Creating:", event.pathname

    def process_IN_DELETE(self, event):
        print "Removing:", event.pathname

# do all the things needed to do when displaying the Home screen:
#    - Change menuShow to False (this clears the display and needs
#         to be the first thing done so the other things take effect)
#    - Set time
#    - Set target/current temp
#    - Set mode
def displayHome():
    global ui, targetTemp

    ui.menuShown = False
    setCurrentTime()
    ui.targetTemp = targetTemp
    parseCurrentTemp()
    parseProgMode()

def parseProgMode():
    global ui

    with open(env.ProgModePath, 'r') as f:
        value = f.read(100)

    if value != '':
        ui.mode = value.rstrip()

def parseCurSysMode():
    with open(env.CurSysModePath, 'r') as f:
        value = f.read(100)

    if value != '':
        setIndicatorLed(value)

def parseTargetTemp():
    global ui, targetTemp

    print "Parsing target temp file"
    with open(env.TargetTempPath, 'r') as f:
        value = f.read(100)

    try:
        targetTemp = int(value)
        ui.targetTemp = int(value)
    except ValueError:
        pass

def parseCurrentTemp():
    global ui

    with open(env.CurrentTempPath, 'r') as f:
        value = f.read(100)

    try:
        ui.currentTemp = float(value)
    except ValueError:
        pass


def onFileUpdate(iFilePath):
    global ui

    if iFilePath in env.ProgModePath:
        parseProgMode()
    # TODO: I don't know why this was in here ... need to validate what the intent of this was
    #       But removing it fixed a bug where the LED turned on when switching modes.
    #elif iFilePath in env.CurSysModePath:
    #    parseCurSysMode()
    elif iFilePath in env.CurrentTempPath:
        parseCurrentTemp()
    elif iFilePath in env.TargetTempPath:
        parseTargetTemp()

def setIndicatorLed(systemMode):
    red = 0
    green = 0
    blue = 0

    if systemMode in "Heat":
        red = 1
    elif systemMode in "Cool":
        blue = 1
    elif systemMode in "Fan":
        green = 1

    with open(env.LedRedPath, 'w') as f:
        f.write(str(red))
    with open(env.LedGreenPath, 'w') as f:
        f.write(str(green))
    with open(env.LedBluePath, 'w') as f:
        f.write(str(blue))

def checkFileUpdates():
    global notifier

    assert notifier._timeout is not None, 'Notifier must be constructed with a short timeout'
    notifier.process_events()
    while notifier.check_events():  #loop in case more events appear while we are processing
        notifier.read_events()
        notifier.process_events()

# initializatin of the UI application
def init():
    global ui, targetTemp, sock, gCurEncoderPos, handler, notifier

    # setup serial port
    if "/dev/" in env.UartPath:
        # if "/dev/" is in the name then we are likely using a real uart
        # so we need to set it up as a true serial port
        serialPort = serial.Serial(port=env.UartPath, baudrate=9600)
    else:
        # otherwise, we are probably dealing with a fake uart which is
        # implemented in the Simulator as a Unix pipe which is implemented
        # in the fakeUart class so that the proper read/write functions are
        # presented to the lower level classes which use this uart
        serialPort = fakeUart(env.UartPath)

    # initialize our local variables to what's in the files
    with open(env.TargetTempPath, 'r') as f:
        value = f.read(100)

    targetTemp = int(value) 

    # initialized UI
    ui = UserInterface.UserInterface(serialPort)
    displayHome()

    # setup file change notifications for current temp, current system mode, and temp mode
    wm = pyinotify.WatchManager()
    mask = pyinotify.IN_MODIFY | pyinotify.IN_DELETE | pyinotify.IN_CREATE  # watched events
    wm.add_watch(env.ProgModePath, mask)
    wm.add_watch(env.CurSysModePath, mask)
    wm.add_watch(env.CurrentTempPath, mask)
    wm.add_watch(env.TargetTempPath, mask)

    handler = EventHandler(onFileUpdate)
    notifier = pyinotify.Notifier(wm, handler, timeout=10)

    # initialize encoder positiion to value from file
    with open(env.EncoderKnobPath) as f:
        pos = f.read(10)

    # convert position to an int ... if this file is not formatted
    # properly then this could throw some nasty exceptions
    gCurEncoderPos = int(pos) / cfg.EncoderDivider

# the "main loop"
def run():
    global sock, ui

    # initialize timer for updating the time on the display
    timeTimer = time.time() + 10

    # initialize timer for rewriting display to clear artifacts
    cleanupTimer = time.time() + 30

    while True:
        time.sleep(0.1)

        # checkButtons and checkEncoder could throw some nasty exceptions if the
        # file format is bad, so lets do my favorite try/except all exceptions
        # just in case something bad goes down
        try:
            checkButtons()
        except Exception, err:
            print traceback.format_exc()

        # just because checkButtons fails, we should still try checkEncoder so
        # it is called in its own try block
        try:
            checkEncoder()
        except Exception, err:
            print traceback.format_exc()

        # if the timer has expired, then set the time and reset the timer
        if time.time() > timeTimer:
            timeTimer = time.time() + 10
            setCurrentTime()
   
        # if the cleanup timer has expired, then clean up the display
        if time.time() > cleanupTimer:
            cleanupTimer = time.time() + 30
            if not ui.menuShown:
                ui.clear()
                displayHome()
            else:
                # TODO: see if we should rewrite display when the menu is shown too
                pass
   
        # check for file modifications
        checkFileUpdates()

# checks for button transitions of all buttons in kButtons
def checkButtons():
    # the key in the kButtons dictionary is the path to the button value
    for buttonPath in kButtons.keys():
        # read value from file
        with open(buttonPath) as f:
            state = f.read(10)

        # convert state to an int ... if this file is not formatted
        # properly then this could throw some nasty exceptions
        state = int(state)

        if state != kButtons[buttonPath]['prevState']:
            # preserve state to detect transitions only
            kButtons[buttonPath]['prevState'] = state

            if state:
                kButtons[buttonPath]['func']()

# checks for encoder movement
def checkEncoder():
    global gCurEncoderPos

    # read value from file
    with open(env.EncoderKnobPath) as f:
        pos = f.read(10)

    # convert position to an int ... if this file is not formatted
    # properly then this could throw some nasty exceptions
    pos = int(pos) / cfg.EncoderDivider

    # if the value is incrementing then the encoder is moving in a 
    # clockwise fashion; if decreasing, then it is moving counter
    # clockwise
    if pos > gCurEncoderPos:
        onCW()
    elif pos < gCurEncoderPos:
        onCCW()

    # preserve position so we can detect movement
    gCurEncoderPos = pos

def setCurrentTime():
    global ui

    ui.time = datetime.datetime.now().time()

def onCCW():
    global ui, targetTemp

    # this file is responsible for incrementing the target temp, but
    # it only does this if the menu is not currently being shown. If the
    # menu is currently shown then it has to notify the UI of the movement
    if ui.menuShown:
        ui.onCCW()
    else:
        targetTemp -= 1
        ui.targetTemp = targetTemp
        with open(env.TargetTempPath, 'w') as f:
            f.write(str(targetTemp))

def onCW():
    global ui, targetTemp

    # this file is responsible for incrementing the target temp, but
    # it only does this if the menu is not currently being shown. If the
    # menu is currently shown then it has to notify the UI of the movement
    if ui.menuShown:
        ui.onCW()
    else:
        targetTemp += 1
        ui.targetTemp = targetTemp
        with open(env.TargetTempPath, 'w') as f:
            f.write(str(targetTemp))

def onSelect():
    global ui

    ui.onSelect()

def onHome():
    displayHome()

def onMenu():
    global ui

    ui.onMenu()

def onBack():
    global ui

    ret = ui.onBack()
    if ret == UIEvents.BACK:
        displayHome()


# dictionary of our buttons and the functions to call when they are pressed
kButtons = { env.EncoderPushPath: { 'func':onMenu, 'prevState':0 },
             env.BackButtonPath:  { 'func':onBack, 'prevState':0 },
             env.HomeButtonPath:  { 'func':onHome, 'prevState':0 }
           }

def main():
    init()
    try:
        run()
    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    main()
