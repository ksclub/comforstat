#!/bin/env python

import Choice
import Environment as env
import UIEvents

class PrgChoice(Choice.Choice):
    def __init__(self, iDisplay, iTitle):
        super(PrgChoice, self).__init__(iDisplay, iTitle, ["Prg", "Hld", "Prg-Hld"])

    def action(self, iChoice):
        with open(env.ProgModePath, 'w') as f:
            f.write(iChoice)

        return UIEvents.BACK

