#!/bin/env python

import Choice
import UIEvents
import Environment as env

class ClockFormatChoice(Choice.Choice):
    def __init__(self, iDisplay, iTitle):
        super(ClockFormatChoice, self).__init__(iDisplay, iTitle, ["12", "24"])

    def action(self, iChoice):
        with open(env.ClockFormatPath, 'w') as f:
            f.write(iChoice)
        
        return UIEvents.BACK

