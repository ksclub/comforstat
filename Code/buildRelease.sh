#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 <version>"
    exit 1
fi

version=$1
baseInstallPath=/var/www/comforstat/releases/
installPath=${baseInstallPath}/${version}

rm -rf build
install -d build/fs/opt/comforstat/python/
cp -r fs_overlay/* build/fs/
cp -r HwInterface Statistics Common UI Thermo Schedule build/fs/opt/comforstat/python/
cp install.sh build/
cp loadSd.sh build/
echo -n "$version" > build/fs/opt/comforstat/version.txt

# remove any .pyc files since I don't always build from a clean environment
find build/ -name "*.pyc" -delete

# make the tarball for the release
pushd build
tar czf ${version}.tgz install.sh loadSd.sh fs/
popd

# now install it onto the web server
rm -rf ${installPath}
install -d ${installPath}
cp build/${version}.tgz ${installPath}

# just assume I don't do something stupid like build a version that is actually
# an older version than what is already the latest
echo ${version} > ${baseInstallPath}/latest.txt

