#!/bin/sh

setupGpio() {
    gpioBasePath="/sys/class/gpio/"
    gpioNum=$1
    gpioDir=$2
    activeLow=$3
    path="$gpioBasePath/gpio$gpioNum"

    if [ ! -d "$path" ]; then
        echo "$gpioNum" > $gpioBasePath/export
    fi

    echo "$gpioDir" > $path/direction

    if [ ! -z "$activeLow" ]; then
        echo "1" > $path/active_low
    fi

    if [ $gpioDir == "out" ]; then
        echo "0" > $path/value
    fi
}

# inputs
# HOME (P9_18)*
setupGpio 4 "in"
# BACK (P9_17)*
setupGpio 5 "in"
# Encode_SW (P9_41)*
setupGpio 20 "in" "low"
# Encode_B (P9_27)*
setupGpio 115 "in"
# Encode_A (P9_42)*
setupGpio 7 "in"

# outputs
# Heat_S+ (P8_10)*
setupGpio 68 "out"
# Cool_S+ (P8_12)*
setupGpio 44 "out"
# Fan_S+ (P8_14)*
setupGpio 26 "out"
# Aux_S+ (P8_16)*
setupGpio 46 "out"
# LED_Red (P8_8)*
setupGpio 67 "out" "low"
# LED_Green (P8_9)*
setupGpio 69 "out" "low"
# LED_Blue (P8_7)*
setupGpio 66 "out" "low"

