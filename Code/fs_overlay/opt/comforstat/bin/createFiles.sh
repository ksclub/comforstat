#!/bin/sh

# exit if things break
set -e

createFile()
{
    FILE=$1
    CONTENTS=$2
    DIR=$(dirname $FILE)

    if [ ! -e $FILE ]; then
        if [ "$CONTENTS" != "" ]; then
            echo $CONTENTS > $FILE
        else
            touch $FILE
        fi
    fi
}

if [ "$1" = "-sim" ]; then
    createFile /apps/interfaces/heatRelay 0
    createFile /apps/interfaces/coolRelay 0
    createFile /apps/interfaces/fanRelay 0
    createFile /apps/interfaces/ledRed 0
    createFile /apps/interfaces/ledGreen 0
    createFile /apps/interfaces/ledBlue 0
    createFile /apps/interfaces/encoderKnob 0
    createFile /apps/interfaces/homeButton 0
    createFile /apps/interfaces/backButton 0
    createFile /apps/interfaces/encoderPush 0
fi

install -d /apps/interfaces/schedules/
createFile /apps/interfaces/targetTemp 68
createFile /apps/interfaces/currentTemp 68
createFile /apps/interfaces/progMode Prg
createFile /apps/interfaces/curSysMode Auto

