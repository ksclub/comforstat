#!/bin/sh

set -e
set -x

cleanup()                                
{                                        
    rm /tmp/latest.txt &>/dev/null || true
    exit $1                               
}                             

host=192.168.101.131                    
releasePath="/comforstat/releases/"            
versionFile="$releasePath/latest.txt"          

rm /tmp/latest.txt &>/dev/null || true    
wget -q http://$host/$versionFile -P /tmp/

newVer=`cat /tmp/latest.txt`    

rm -rf /tmp/update
mkdir /tmp/update
wget -q http://$host/$releasePath/$newVer/${newVer}.tgz -P /tmp/update

cd /tmp/update
gunzip ${newVer}.tgz
tar xf ${newVer}.tar

# install must be put into the background so that this script can finish
# execution and will allow the file system to be remounted within the
# install script
./install.sh &
cd -

