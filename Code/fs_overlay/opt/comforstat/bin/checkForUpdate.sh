#!/bin/sh

set -e                                   

cleanup()                                
{                                        
    rm /tmp/latest.txt &>/dev/null || true
    exit $1                               
}                             

host=192.168.101.131                    
path="/comforstat/releases/"            
versionFile="$path/latest.txt"          

curVer=`cat /opt/comforstat/version.txt`  

rm /tmp/latest.txt &>/dev/null || true    
wget -q http://$host/$versionFile -P /tmp/

newVer=`cat /tmp/latest.txt`    

if [ $newVer \> $curVer ]; then     
    echo "New version available"    
    cleanup 1                       
else                                
    echo "Already at latest version"
    cleanup 0                       
fi           

