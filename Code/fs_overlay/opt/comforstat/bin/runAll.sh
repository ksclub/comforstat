#!/bin/sh

. /opt/comforstat/environment

runOneWireTemp.sh > /tmp/tempReader.log 2>&1  &
runThermo.sh > /tmp/thermostat.log 2>&1 &
runScheduler.sh > /tmp/scheduler.log 2>&1 &
runUI.sh > /tmp/userInterface.log 2>&1 &
runStats.sh > /tmp/statistics.log 2>&1 &

