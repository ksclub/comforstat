#!/bin/env python

import time
import datetime
import json

import pyinotify

import Environment as env

# list of the days of the week to use when doing a lookup from the return
# value of weekday() in the datetime library
kDaysOfWeek = [ 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ]

# This EventHandler class is used for handling events related to file
# change notifications
class EventHandler(pyinotify.ProcessEvent):
    def __init__(self, callback):
        self.callback = callback

    def process_IN_MODIFY(self, event):
        #print "Modified:", event.pathname
        self.callback(event.pathname)

    def process_IN_CREATE(self, event):
        #print "Created:", event.pathname
        pass

    def process_IN_DELETE(self, event):
        #print "Removed:", event.pathname
        pass

class Scheduler(object):
    def __init__(self):
        self.schedule = None
        self.scheduleTimeAtHoldTransition = None
        self.setTargetTemp = 0
        self.currentScheduleWd = -1
        self.scheduleFilePath = ""

        with open(env.ActiveSchedulePath) as f:
            self.scheduleFilePath = f.readline().rstrip()

        self.parseSchedule()

        # setup file change notifications for the schedule
        self.wm = pyinotify.WatchManager()
        mask = pyinotify.IN_MODIFY | pyinotify.IN_DELETE | pyinotify.IN_CREATE  # watched events
        #print "File to watch:",env.ScheduleFilePath
        self.wm.add_watch(env.ActiveSchedulePath, mask)
        self.wm.add_watch(env.TargetTempPath, mask)
        self.replaceScheduleWatchPath()

        self.handler = EventHandler(self.onFileUpdate)
        self.notifier = pyinotify.Notifier(self.wm, self.handler, timeout=10)

    def replaceScheduleWatchPath(self):
        mask = pyinotify.IN_MODIFY | pyinotify.IN_DELETE | pyinotify.IN_CREATE  # watched events
        wdDict = self.wm.add_watch(env.ScheduleFolderPath + "/" + self.scheduleFilePath, mask)

        for key in wdDict.keys():
            if key == self.scheduleFilePath:
                self.currentScheduleWd = wdDict[key]
                break

    def parseActiveSchedule(self):
        print "Reading active schedule"

        with open(env.ActiveSchedulePath) as activeScheduleFile:
            path = activeScheduleFile.read().rstrip()

            if path != "":
                self.scheduleFilePath = path
                self.replaceScheduleWatchPath()
                self.parseSchedule()
            else:
                print "   ERROR: Active schedule path was empty"

    def parseSchedule(self):
        print "Reading schedule"
        try:
            # read in schedule file and store it
            with open(env.ScheduleFolderPath + "/" + self.scheduleFilePath) as scheduleFile:
                self.schedule = json.load(scheduleFile)['schedule']
        except:
            print "Failed to read schedule (%s) ... oops"%self.scheduleFilePath

    def parseTargetTemp(self):
        #print "Target temp was changed"
        if self.getProgMode().startswith("Hld"):
            return
        else:
            # check what the current target temp is
            with open(env.TargetTempPath) as file:
                targetTemp = int(file.read(100))

            #print "setTemp: '%d' targetTemp: '%d'"%(int(self.setTargetTemp), int(targetTemp))
            if self.setTargetTemp != targetTemp:
                print "Putting into Prg-Hld mode"
                self.scheduleTimeAtHoldTransition = self.getScheduleTime()
                self.setProgMode("Prg-Hld")

    def getProgMode(self):
        # read in temp mode file and store it
        with open(env.ProgModePath) as modeFile:
            mode = modeFile.read(100)

        return mode

    def setProgMode(self, iMode):
        # write to temp mode file and store it
        with open(env.ProgModePath, 'w') as modeFile:
            mode = modeFile.write(iMode)

    def checkFileUpdates(self):
        assert self.notifier._timeout is not None, 'Notifier must be constructed with a short timeout'
        self.notifier.process_events()
        #print "Checking files"
        while self.notifier.check_events():  #loop in case more events appear while we are processing
            self.notifier.read_events()
            self.notifier.process_events()

    def onFileUpdate(self, iFilePath):
        #print "Something is happening"
        if iFilePath in self.scheduleFilePath:
            self.parseSchedule()
        elif iFilePath in env.TargetTempPath:
            self.parseTargetTemp()
        elif iFilePath in env.ActiveSchedulePath:
            self.parseActiveSchedule()

    def getTimeList(self, dayOfWeek):
        times = []

        # get all time/temp entries from the config
        for entry in self.schedule[dayOfWeek]:
            times.append(entry['time'])
    
        # things expect this list to be sorted
        times.sort()

        return times

    def getTempAtTime(self, time, dayOfWeek):
        for entry in self.schedule[dayOfWeek]:
            if time in entry['time']:
                # found it ... lets return that value
                return entry['temp']

        # if we got here then we couldn't find the time and that shouldn't happen
        raise Exception("Time (%s) not found on %s"%(time,dayOfWeek))

    def getTimeTemp(self):
        # Get day of week: weekday() returns 0 to 6 for monday to sunday respectively
        today = kDaysOfWeek[datetime.datetime.now().weekday()]

        # get a sorted list of times from the schedule
        #print "today:", today
        times = self.getTimeList(today)

        # get current time in same format that our schedule file stores them
        curTime = datetime.datetime.now().strftime("%H:%M")
            
        # special case to handle time between midnight and first scheduled temp
        # this will take the temp from the last time of the previous day
        if curTime < times[0]:
            # this gets the name of the day for yesterday. This works because
            # python interprets array[-1] as the last item of the array
            yesterday = kDaysOfWeek[datetime.datetime.now().weekday() - 1]

            # get sorted list of times from yesterday and get last
            # the last time slot (that's the [-1])
            scheduleTime = self.getTimeList(yesterday)[-1]

            # get temp from that last time slot
            targetTemp = self.getTempAtTime(scheduleTime, yesterday)
        else:
            # if we exit the loop below then we must be in the final time slot,
            # so here we will assume that we are in that slot
            scheduleTime = times[-1]

            #print "CurTime:", curTime
            #print "len:", range(1, len(times))

            # loop through all the times for today and see if we can find which
            # time slot we are in
            for i in range(1, len(times)):
                #print "times(i-1):", times[i - 1]
                #print "times(i):", times[i]

                # if we are greater than or equal to time in slot i - 1, but less
                # than time slot i ... then we must be in time slot i - 1
                if times[i - 1] <= curTime and curTime < times[i]:
                    # so we will save off the time value to look up later
                    scheduleTime = times[i - 1]
                    break
    
            # now we look up the target temp in the schedule for our time slot
            #print "scheduleTime:", scheduleTime
            targetTemp = self.getTempAtTime(scheduleTime, today)

        return (scheduleTime, targetTemp)

    def getScheduleTime(self):
        (scheduleTime,_) = self.getTimeTemp()
        return scheduleTime

    def getTargetTemp(self):
        (_,targetTemp) = self.getTimeTemp()
        return targetTemp

    def determineTargetTemp(self):
        if self.getProgMode().startswith("Hld"):
            # if we are in hold, the scheduler really shouldn't be doing much
            # of anything. So just return here w/o doing anything
            #print "In Hld ... don't do anything"
            return
        elif "Prg-Hld" == self.getProgMode():
            # if we are in program-hold (this means that someone temporarily
            # changed the temp using the knob w/o specifically telling us to
            # hold it forever), then we need to see if it's time to kick out
            # of this mode once the "next" schedule cycle comes around
            #print "In Prg-Hld ... transTime:",self.scheduleTimeAtHoldTransition,"schedTime:",self.getScheduleTime()
            if self.scheduleTimeAtHoldTransition != self.getScheduleTime():
                print "Going back to Prg mode"
                self.setProgMode("Prg")
        else:
            # we are in normal program mode so just go about our business as
            # normal. This means determining what our target temp should be
            # and then setting it in the target temp file if it has changed
            #print "In Prg ... do your normal thing"
            targetTemp = self.getTargetTemp()

            # check what the current target temp is
            with open(env.TargetTempPath) as file:
                currentTargetTemp = file.read(100)

            try:
                targetTemp = int(targetTemp)
                currentTargetTemp = int(currentTargetTemp)
            except ValueError:
                pass
            else:
                # see if we have changed our target temp
                if targetTemp != currentTargetTemp:
                    self.setTargetTemp = targetTemp
                    print "Changing target temp to", targetTemp

                    # change target temp for thermostat
                    with open(env.TargetTempPath, "w") as file:
                        file.write(str(targetTemp))

    def run(self):
        while True:
            # checkFileUpdates must be called first to make sure we don't clobber
            # an update that we get from the user interface
            self.checkFileUpdates()
            self.determineTargetTemp()

            # sleep for a bit to save CPU cycles
            time.sleep(1)


if __name__ == "__main__":
    scheduler = Scheduler()
    scheduler.run()
