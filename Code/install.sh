#!/bin/sh

set -e
set -x

installDir="/opt/comforstat/"
pythonInstallDir="$installDir/python/"
scriptInstallDir="$installDir/bin/"
initTabContents="::respawn:${scriptInstallDir}/runOneWireTemp.sh\n::respawn:${scriptInstallDir}/runScheduler.sh\n::respawn:${scriptInstallDir}/runThermo.sh\n::respawn:${scriptInstallDir}/runUI.sh\n::respawn:${scriptInstallDir}/runStats.sh\n"

# remount as read/write
mount -o remount,rw /

# clear out the old files
rm -rf ${installDir} || true

# install files
cp -r fs/opt/* /opt/

# setup init script
rm -f /etc/init.d/S51comforstat
ln -s ${installDir}/init.d/rcS /etc/init.d/S51comforstat

# create respawn tasks in /etc/inittab for all our apps
grep -q "runOneWireTemp" /etc/inittab

if [ $? -ne 0 ]; then
    sed -i "s#\(/etc/init.d/rcS\)#\1\n${initTabContents}#" /etc/inittab
fi

# clean up
killall python
sleep 5
find /opt/comforstat -name "*.pyc" -exec rm {} \;
sync
sleep 5
mount -o remount,ro /
echo "Upgrade has completed ..."

