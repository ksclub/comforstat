#!/bin/env python
import time
import datetime

import Environment as env
from FreeMem import FreeMemLinux

def getValueFromFile(fileName):
    with open(fileName) as f:
        value = f.readline().rstrip()

    return value

def getUptime():
    with open('/proc/uptime', 'r') as f:
        seconds = int(f.readline().split()[0].split(".")[0])

    return str(datetime.datetime.utcfromtimestamp(seconds))

def getFreeMem():
    memGetter = FreeMemLinux()
    return str(memGetter.user_free)

def boolToStr(boolValue):
    return "1" if boolValue else "0"

sleepTime = 10
inactiveStatPeriod = 300
activeStatPeriod = 60
#sleepTime = 1
#inactiveStatPeriod = 30
#activeStatPeriod = 6

isActive = False

nextStatGatherTime = time.time() + activeStatPeriod

while True:
    heatRelay = True if (getValueFromFile(env.HeatRelayPath) == "1") else False
    coolRelay = True if (getValueFromFile(env.CoolRelayPath) == "1") else False
    fanRelay = True if (getValueFromFile(env.FanRelayPath) == "1") else False
    auxRelay = True if (getValueFromFile(env.AuxRelayPath) == "1") else False

    currentlyActive = (heatRelay or coolRelay or fanRelay or auxRelay)
    #print heatRelay, coolRelay, fanRelay, auxRelay, currentlyActive

    if currentlyActive and not isActive:
        netStatGatherTime = time.time()
    elif not currentlyActive and isActive:
        netStatGatherTime = time.time()

    isActive = currentlyActive

    #print "Now:", time.time(), "next:", nextStatGatherTime
    if time.time() > nextStatGatherTime:
        #print "Gettin stats"

        date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        uptime = getUptime()
        freeMem = getFreeMem()
        redLed = True if (getValueFromFile(env.LedRedPath) == "1") else False
        blueLed = True if (getValueFromFile(env.LedBluePath) == "1") else False
        greenLed = True if (getValueFromFile(env.LedGreenPath) == "1") else False
        currentTemp = getValueFromFile(env.CurrentTempPath)
        targetTemp = getValueFromFile(env.TargetTempPath)
        progMode = getValueFromFile(env.ProgModePath)
        sysMode = getValueFromFile(env.CurSysModePath)

        stats = ",".join([date, uptime, freeMem, currentTemp, targetTemp, boolToStr(currentlyActive), boolToStr(heatRelay), boolToStr(coolRelay), boolToStr(fanRelay), boolToStr(auxRelay), boolToStr(redLed), boolToStr(greenLed), boolToStr(blueLed), progMode, sysMode])

        with open('/apps/logs/log-%s.csv'%date.split()[0], 'a') as f:
            f.write(stats + "\n")

        if isActive:
            nextStatGatherTime = time.time() + activeStatPeriod
        else:
            nextStatGatherTime = time.time() + inactiveStatPeriod

    time.sleep(sleepTime)
