#!/bin/env python

import time
import Environment as env
import Config as cfg

# read the temp with no filtering
def readTemp():
    with open(env.DigThermPath, "r") as file:
        data = file.readlines()

    # this is what the data looks like in the temp file:
    #    b0 01 4b 46 7f ff 10 10 3a : crc=3a YES
    #    b0 01 4b 46 7f ff 10 10 3a t=27000
    #
    # below we are going to search for the "t=" part and grab the digits
    # after it
    data = ''.join(data).replace("\n", "")
    data = data[data.rfind("t=") + 2:]

    # The temp is reported in the t=<number> part of this. The units on the
    # temp are milli degrees C. To get this in deg F, we must divide by 1000
    # (27), and then do C -> F conversion (80.6)
    data = ((int(data)/1000.0) * 9.0 / 5.0) + 32.0

    # through testing, it seems the temp may be reading off by a somewhat
    # consistent offset. So this is the fudge factor to get it back in line
    data += cfg.TempFudgeFactor

    return data

# prime the prev data with the current temp on startup of this app
prevData = readTemp()

while True:
    # wait some time before next read
    time.sleep(cfg.TempReadPeriod)

    try:
        # filter data using a weighted average with weights from config
        data = (prevData * cfg.TempPrevWeight) + (readTemp() * cfg.TempCurWeight)
    except ValueError:
        # if the file read gets garbage the int(...) conversion will throw a
        # ValueError, so catch it here and act like nothing happened
        continue
    else:
        # save off current value for next reading
        prevData = data

        # if the call to readTemp(...) didn't throw an exception, then we need
        # to write data out to the current temp file
        with open(env.CurrentTempPath, "w") as file:
            file.write(str(data))

