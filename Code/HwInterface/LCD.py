#!/bin/evn python
import time

kBrightness = { 0: 128, 40: 140, 73: 150, 100: 157 }

CLEAR_DISPLAY       = "\xFE\x01" #[ 0xFE, 0x01 ]
CURSOR_RIGHT_ONE    = [ 0xFE, 0x14 ]
CURSOR_LEFT_ONE     = [ 0xFE, 0x10 ]
SCROLL_RIGHT        = [ 0xFE, 0x1C ]
SCROLL_LEFT         = [ 0xFE, 0x18 ]
DISPLAY_ON          = [ 0xFE, 0x0C ]
DISPLAY_OFF         = [ 0xFE, 0x08 ]
UNDERLINE_ON        = [ 0xFE, 0x0E ]
UNDERLINE_OFF       = [ 0xFE, 0x0C ]
BLINK_CURSOR_ON     = [ 0xFE, 0x0D ]
BLINK_CURSOR_OFF    = [ 0xFE, 0x0C ]
SET_CURSOR_POSITION = [ 0xFE, 0x80 ]

class LCD():
    def __init__(self, iPort=None):
        self.ser = iPort
        self.curPos = 0
        self.ser.write("\x7C" + chr(kBrightness[40]))
        time.sleep(0.1)
        self.ser.write("\xFE\x0C")

    def writeText(self, iText, iColumn, iRow):
        self.setPosition(iColumn, iRow)
        self.ser.write(iText)
        self.curPos += len(iText)

    def setPosition(self, iColumn, iRow):
        pos = (iRow * 64) + iColumn

        # make sure we are in the range for our 16 x 2 LCD
        if pos > 79 or (pos > 15 and pos < 64) or pos < 0:
            raise Exception("Index out of range")

        if pos == self.curPos:
            return

        # add in command indication
        pos += 0x80

        # send command out
        self.ser.write("\xFE" + chr(pos))

    def clear(self):
        self.ser.write(CLEAR_DISPLAY)

    def sendCommand(self, command):
        self.ser.write(command)

