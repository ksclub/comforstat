#!/bin/env python

import Environment as env

class LED(object):
    def __init__(self, iPath):
        self._isOn = False
        self.path = iPath

    @property
    def isOn(self):
        return self._isOn

    @isOn.setter
    def isOn(self, value):
        if not value in [True, False]:
            raise Exception("State %s not supported"%value)

        self._isOn = value

        with open(self.path, "w") as file:
            file.write("1" if self._isOn else "0")

red = LED(env.LedRedPath)
green = LED(env.LedGreenPath)
blue = LED(env.LedBluePath)

