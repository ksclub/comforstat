#!/bin/evn python
kBrightness = { 0: 128, 40: 140, 73: 150, 100: 157 }

CLEAR_DISPLAY       = [ 0xFE, 0x01 ]
CURSOR_RIGHT_ONE    = [ 0xFE, 0x14 ]
CURSOR_LEFT_ONE     = [ 0xFE, 0x10 ]
SCROLL_RIGHT        = [ 0xFE, 0x1C ]
SCROLL_LEFT         = [ 0xFE, 0x18 ]
DISPLAY_ON          = [ 0xFE, 0x0C ]
DISPLAY_OFF         = [ 0xFE, 0x08 ]
UNDERLINE_ON        = [ 0xFE, 0x0E ]
UNDERLINE_OFF       = [ 0xFE, 0x0C ]
BLINK_CURSOR_ON     = [ 0xFE, 0x0D ]
BLINK_CURSOR_OFF    = [ 0xFE, 0x0C ]
SET_CURSOR_POSITION = [ 0xFE, 0x80 ]

class FakeLCD():
    def __init__(self, iPort=None, iBaudrate=9600):
        self.display = iPort
        self.cycleCount = 0
        self.blinkTextData = None
        self.blinkPos = None

    def blinkCycle(self):
        self.cycleCount += 1

        if self.blinkTextData and self.blinkPos:
            (col, row) = self.blinkPos
            position = self.display.XYToPosition(col, row)
            textLen = len(self.blinkTextData)
            blankText = " " * textLen
            text = blankText if (self.cycleCount % 2) == 0 else self.blinkTextData
            self.display.Replace(position, position + textLen, text)

    def blinkText(self, iText, iColumn, iRow):
        self.blinkTextData = iText
        self.blinkPos = (iColumn, iRow)

    def writeText(self, iText, iColumn, iRow):
        position = self.display.XYToPosition(iColumn, iRow)

        self.display.Replace(position, position + len(iText), iText)
        self.display.Refresh()

    def setPosition(self, iColumn, iRow):
        self.display.SetInsertionPoint(self.display.XYToPosition(iColumn, iRow))

    def clear(self):
        self.display.Clear()
        self.display.WriteText("                \n                ")
        self.blinkTextData = None
        self.blinkPos = None

    def sendCommand(self, command):
        raise Exception("This function not implemented")

