#!/bin/env python
import Adafruit_BBIO.UART as UART
import serial
import time
 
UART.setup("UART1")
  
ser = serial.Serial(port = "/dev/ttyO1", baudrate=9600)
ser.close()
ser.open()
if ser.isOpen():
   print "Serial is open!"
   command = [ 0x7C, 140, 0xFE, 0x01, 0xFE, 0x80, 0xFE, 0x0D ]
   for c in command:
      ser.write(chr(c))
      print "writing out %x"%c
      time.sleep(0.02)

   while 1:
      for c in "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef":
         ser.write(c)
   ser.close()

